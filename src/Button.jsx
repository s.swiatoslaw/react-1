import {Component} from "react"

class Button extends Component {
    render() {
        const {color, text, onclick} = this.props;
        return <button onClick={onclick} style={{backgroundColor: color}}>{text}</button>
    }
}

export default Button