import Modal from './Modal'
import Button from './Button'
import {Component} from 'react'
import './main.scss'
import ReactDOM from 'react-dom'
class App extends Component {
  constructor(){
    super();
    this.state = {
      isShowFirst: false,
      isShowSecond: false,
      class: "none"
    }
  }

  toggleFirst = () => {
    this.setState(() => ({
      isShowFirst: !this.state.isShowFirst
    }))
      if (this.state.class === "none") {
        this.setState({class: 'block'});
      } else {
        this.setState({class: "none"})
      }
  }
  toggleSecond = () => {
    this.setState(() => ({
      isShowSecond: !this.state.isShowSecond
    }))
    if (this.state.class === "none") {
      this.setState({class: 'block'});
    } else {
      this.setState({class: "none"})
    }
  }
  closeModal = (event) => {
    if (!event.target.classList.contains('modal')) {
      this.setState(() => ({
        isShowFirst: !this.state.isShowFirst
      }))
      }
  }
  render(){
    return(
      <>
        {this.state.isShowFirst === true && (
         <Modal classStyle={ {display: this.state.class}} header="Do you want to delete this file?" text="Modal #1" closeButton closeModal={this.toggleFirst}
         actions= {
           <>
              <Button onclick={this.toggleFirst} text="Enter"/>
              <Button onclick={this.toggleFirst} text="Close"/>
           </>
         }/>
        )}
        {this.state.isShowSecond === true && (
         <Modal header="Do you want to add this file?" text="Modal #2" closeButton closeModal={this.toggleSecond}
         actions= {
           <>
              <Button onclick={this.toggleSecond} text="Enter"/>
              <Button onclick={this.toggleSecond} text="Close"/>
           </>
         }/>
        )}
        <div className="buttons">
        <Button onclick={this.toggleFirst} color="red" text="Open first modal"></Button>
        <Button onclick={this.toggleSecond} color="blue" text="Open second modal"></Button>
        </div>
      </>
    )
  }
}

export default App;
