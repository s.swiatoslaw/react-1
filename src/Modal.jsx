import {Component} from "react"
import CloseIcon from '@material-ui/icons/Close';
import Button from './Button'
class Modal extends Component {

    render() {
        const {header, closeButton, text, actions, closeModal, classStyle} = this.props;
        return (
            <div onClick={closeModal} style={classStyle} className="modal">
                                <div className="modal-window">
                                <div className="modal-header">
                                <h1>
                                    {header}
                                </h1>
                                <CloseIcon onClick={closeModal} style={{color: "#FFFFFF"}} fontSize="large"/>
                            </div>
                            <div className="modal-body">
                                <p>{text}</p>
                            </div>
                            <div className="modal-footer">
                                {actions}
                            </div>
                                </div>
                                </div>
        );
    }
}

export default Modal